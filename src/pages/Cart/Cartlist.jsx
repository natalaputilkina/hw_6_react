import { useOutletContext } from 'react-router-dom';
import { useState } from 'react';
import './Cartlist.scss'
import ProductsList from '../../components/ProductsList/ProductsList';
import { useSelector, useDispatch } from 'react-redux';
import { clearCart } from '../../shopSlice';
import LoginForm from '../../components/LoginForm/LoginForm';






function CartList(){

    const cart = useSelector((state) => state.shop.cart);
    const { products, favorite } = useOutletContext();
    const dispatch = useDispatch();
    const [isModalOpen, setIsModalOpen] = useState(false);

    const handleCheckoutClick = () => {
        setIsModalOpen(true);
    };

    const handleCloseModal = () => {
        setIsModalOpen(false);
    };

    const handleFormSubmit = (values) => {
        console.log('інформація про придбані товари', cart);
        console.log('інформацію про користувача', values);
        dispatch(clearCart());
        setIsModalOpen(false);
    };

    if (cart.length === 0){
        return (

            <>
                <h1>Cart</h1>
                <h2>Ваш кошик порожній, але можете його поповнити</h2>

            </>
        
        )
    }

    const cartProducts = products.filter((product) => cart.some((cartItem) => cartItem.id === product.id));

    // console.log(cartProducts)


    return (
        
            <>
                <div className='cart-title-conteiner'>
                    <h1 className='title'>Cart</h1>
                    <button className="button_modal" onClick={handleCheckoutClick}>Checkout</button>
                </div>
                
                <ProductsList
                products={cartProducts}                
                favorite={favorite}
                cart={cart}
                />
                {isModalOpen && <LoginForm onClose={handleCloseModal} onSubmit={handleFormSubmit} />}
                
            </>
        
    )

        
    
}

export default CartList
