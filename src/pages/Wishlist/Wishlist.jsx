import { useOutletContext } from 'react-router-dom';
import './Wishlist.scss'
import ProductsList from '../../components/ProductsList/ProductsList';
import { useSelector } from 'react-redux';



function WishList(){

    const favorite = useSelector((state) => state.shop.favorite);
    const { products, cart } = useOutletContext();

    


    // const fav = JSON.parse(localStorage.getItem('favorite')) || []
    
    if (favorite.length === 0){
        return (

            <>
                <h1>Wishlist</h1>
                <h2>Ваш список бажань порожній, але можете його поповнити</h2>

            </>
        
        )
    }

    const WishListProducts = products.filter((product) => favorite.some((favoriteItem) => favoriteItem.id === product.id));


    return (   
        <>
            <h1 className='title'>Wishlist</h1>
            <div className="wishList">
                <ProductsList
                products={WishListProducts}                
                favorite={favorite}
                cart={cart}
                 />
            </div>     
        </>
              

    )
    
}

export default WishList


