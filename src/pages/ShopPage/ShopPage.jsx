import { useOutletContext } from 'react-router-dom';
import "./ShopPage.scss"
import ProductsList from '../../components/ProductsList/ProductsList';
import ViewSwitcher from '../../components/ViewSwitcher/ViewSwitcher';





function ShopPage(){

   

    const { products, cart, favorite } = useOutletContext();

     return (
        <>
            
                <div className='conteiner-title'>
                    <h1 className='title'>Shop page</h1>
                    <ViewSwitcher />
                </div>
                
                <div className="shop--page">
                    <ProductsList
                        className="shop--page"
                        products={products}                    
                        favorite={favorite}
                        cart={cart}
                    />
                </div> 
            
        </>   
                  

    )
    

}
export default ShopPage