import shopReducer, {
    addToCart,
    removeFromCart,
    addToFavorite,
    removeFromFavorite,
    initializeCart,
    initializeFavorite,
    clearCart
  } from './shopSlice';
  
  const initialState = {
    cart: [],
    favorite: [],
    isModalOpen: false,
  };
  
  describe('shopSlice', () => {
    test('should handle initial state', () => {
      expect(shopReducer(undefined, {})).toEqual(initialState);
    });
  
    test('should handle addToCart', () => {
      const product = { id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' };
      const action = { type: addToCart.type, payload: product };
      const state = shopReducer(initialState, action);
      expect(state.cart).toContainEqual(product);
    });
  
    test('should handle removeFromCart', () => {
      const product = { id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' };
      const initialStateWithProduct = { ...initialState, cart: [product] };
      const action = { type: removeFromCart.type, payload: product };
      const state = shopReducer(initialStateWithProduct, action);
      expect(state.cart).not.toContainEqual(product);
    });
  
    test('should handle addToFavorite', () => {
      const product = { id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' };
      const action = { type: addToFavorite.type, payload: product };
      const state = shopReducer(initialState, action);
      expect(state.favorite).toContainEqual(product);
    });
  
    test('should handle removeFromFavorite', () => {
      const product = { id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' };
      const initialStateWithProduct = { ...initialState, favorite: [product] };
      const action = { type: removeFromFavorite.type, payload: product };
      const state = shopReducer(initialStateWithProduct, action);
      expect(state.favorite).not.toContainEqual(product);
    });
  
    test('should handle initializeCart', () => {
      const cart = [{ id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' }];
      const action = { type: initializeCart.type, payload: cart };
      const state = shopReducer(initialState, action);
      expect(state.cart).toEqual(cart);
    });
  
    test('should handle initializeFavorite', () => {
      const favorite = [{ id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' }];
      const action = { type: initializeFavorite.type, payload: favorite };
      const state = shopReducer(initialState, action);
      expect(state.favorite).toEqual(favorite);
    });
  
    test('should handle clearCart', () => {
      const initialStateWithCart = { ...initialState, cart: [{ id: '1', name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB' }] };
      const action = { type: clearCart.type };
      const state = shopReducer(initialStateWithCart, action);
      expect(state.cart).toEqual([]);
    });
  });