import "./ModalHeader.scss"
// import ModalImage from "./ModalImage"


function ModalHeader ({children}){
    return <div className="modal__header">
        {children}
    </div>
}

export default ModalHeader