import "./ModalText.scss"



function ModalText ({title, text}){
    
    return (
        <div>
            <p className="modal__text-title">{title}</p>
            <p className="modal__text">{text}</p>
        </div>
        
    )

    
    
    
}
export default ModalText