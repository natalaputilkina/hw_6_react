import './ProductsList.scss'
import ProductCard from '../ProductCard/ProductCard';
import { useSelector } from 'react-redux';
import { useContext } from 'react';
import { ViewContext } from '../../ViewContext.jsx';


const ProductsList = ({ products}) => {

    const cart = useSelector((state) => state.shop.cart);
    const favorite = useSelector((state) => state.shop.favorite);
    const { view } = useContext(ViewContext);

    const listClass = view === 'lines' ? 'products-list2' : 'products-list';
    const productClass = view === 'lines' ? 'product2' : 'product';

    return (
        <div className={listClass}>
            {products.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}
                    productClass={productClass}                    
                    isFavorite={favorite.some((item) => item.id === product.id)}
                    isInCart={cart.some((item) => item.id === product.id)}
                />
            ))}
        </div>
    );
};

export default ProductsList;