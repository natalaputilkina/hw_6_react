import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import ProductCard from './ProductCard';


const mockProduct = {
  id: 1,
  name: 'Мобільний телефон Apple iPhone 15 Pro Max 256GB',
  imageUrl: 'https://content2.rozetka.com.ua/goods/images/big/364834252.jpg',
  price: '52.999',
  color: 'Natural Titanium'
};


const mockStore = configureStore([]);
const initialState = {
  products: {
    items: [mockProduct],
    status: 'succeeded',
  },
  shop: {
    cart: [],
    favorite: [],
  },
};
const store = mockStore(initialState);

const renderWithProvider = (component) => {
  return render(
    <Provider store={store}>
      {component}
    </Provider>
  );
};

describe('ProductCard', () => {
  test('renders product name', () => {
    renderWithProvider(<ProductCard product={mockProduct} isFavorite={false} isInCart={false} />);
    const productName = screen.getByText(/Мобільний телефон Apple iPhone 15 Pro Max 256GB/i);
    expect(productName).toBeInTheDocument();
  });

  test('renders add to cart button when product is not in cart', () => {
    renderWithProvider(<ProductCard product={mockProduct} isFavorite={false} isInCart={false} />);
    const addToCartButton = screen.getByText(/Add to cart/i);
    expect(addToCartButton).toBeInTheDocument();
  });

  test('renders remove from cart button when product is in cart', () => {
    renderWithProvider(<ProductCard product={mockProduct} isFavorite={false} isInCart={true} />);
    const removeFromCartButton = screen.getByText(/Remove from cart/i);
    expect(removeFromCartButton).toBeInTheDocument();
  });

  test('calls addToFavoriteHandler when favorite button is clicked', () => {
    renderWithProvider(<ProductCard product={mockProduct} isFavorite={false} isInCart={false} />);
    const favoriteButton = screen.getByRole('button', { name: /Favorite/i });
    fireEvent.click(favoriteButton);
    expect(screen.getByRole('button', { name: /Favorite/i })).toBeInTheDocument();
  });

  test('calls removeFromFavoriteHandler when favorite button is clicked', () => {
    renderWithProvider(<ProductCard product={mockProduct} isFavorite={true} isInCart={false} />);
    const favoriteButton = screen.getByRole('button', { name: /Favorite/i });
    fireEvent.click(favoriteButton);
    expect(screen.getByRole('button', { name: /Favorite/i })).toBeInTheDocument();
  });
});
