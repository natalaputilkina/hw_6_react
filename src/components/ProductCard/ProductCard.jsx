import "./ProductCard.scss"
import ModalPage from "../MainPage/ModalPage";
import IconStar from "../../assets/star.svg"
import starFavoritIcon from "../../assets/star-favorit.svg";
import Button from "../Button";
import { useDispatch } from "react-redux";
import { addToCart, removeFromCart, addToFavorite, removeFromFavorite } from '../../shopSlice';



function ProductCard ({product, isFavorite, isInCart, productClass}){

    const dispatch = useDispatch()

    const addToCartHandler = () => {
        dispatch(addToCart(product));
      };
    
    const addToFavoriteHandler = () => {
        dispatch(addToFavorite(product));
    };

    const removeFromFavoriteHandler = () => {
        dispatch(removeFromFavorite(product));
    };

    const {id, name, imageUrl, price, color} = product

    return (
        <div className={productClass}>
            <img className="product--img" src={imageUrl}></img>
            <h2 className="product--name">{name}</h2>             
            <p className="product--price">Price {price} $</p>
            <p className="product--article">Article {id}</p>
            <div className="product--color">{color}</div>
            {isInCart ? (
                <Button classNames="button_modal" disabled title="In cart" />
            ) : (
                <ModalPage 
                    type="first" 
                    title="Add to cart" 
                    text={`By clicking the “Yes, add” button, ${name} will be added to cart.`} 
                    secondaryText="YES, ADD" 
                    click={addToCartHandler}
                />
            )}
            
            <button className="favorite__button" onClick={isFavorite ? removeFromFavoriteHandler : addToFavoriteHandler}>
                <img className="favorite--image" src={isFavorite ? starFavoritIcon : IconStar} alt="Favorite" />
            </button>

            {isInCart && (
                <ModalPage 
                    type="first" 
                    title="Remove from cart" 
                    text={`By clicking the “Yes, remove” button, ${name} will be removed from cart.`} 
                    secondaryText="YES, REMOVE" 
                    click={() => dispatch(removeFromCart(product))}
                />
            )}
            
        </div>
    )
}


export default ProductCard