import "./ViewSwitcher.scss"
import linesIcon from "../../assets/lines.svg"
import squaresIcon from "../../assets/squares.svg"
import { useContext } from "react";
import { ViewContext } from "../../ViewContext.jsx";


function ViewSwitcher (){

    const { view, toggleView } = useContext(ViewContext);

    return (
        <div className="switcher-conteiner">
            <button className="switcher-button" onClick={() => toggleView('lines')}>
                <img className="switcher-img" src={linesIcon} />
            </button>

            <button className="switcher-button" onClick={() => toggleView('squares')}>
                <img className="switcher-img" src={squaresIcon} />
            </button>

        </div>
    )

}

export default ViewSwitcher


