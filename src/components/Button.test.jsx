import { render } from '@testing-library/react';
import Button from './Button';

test('renders Button component correctly', () => {
  const { asFragment } = render(<Button title="Click me" />);
  expect(asFragment()).toMatchSnapshot();
});