import "./LoginForm.scss"
import { Formik, Form, Field, ErrorMessage } from "formik"
import * as Yup from 'yup'


const validationSchema = Yup.object().shape({
    nameUser: Yup.string().required('Потрібно заповнити Ваше  Ім\'я').matches(/^[a-zA-Za-яА-Я]*$/, "Тільки літери"),
    surnameUser: Yup.string().required('Поле обов\'язкове до заповнення').matches(/^[a-zA-Za-яА-Я]*$/, "Тільки літери"),
    ageUser: Yup.number().positive().integer().required('Поле обов\'язкове до заповнення'),
    adressUser: Yup.string().required('Поле обов\'язкове до заповнення'),
    phoneUser: Yup.number().required('Поле обов\'язкове до заповнення'),
})

const LoginForm = ({onClose, onSubmit}) => (
    <>
        

        <Formik

            initialValues={{
                nameUser: "",
                surnameUser: "",
                ageUser: "",
                adressUser: "",
                phoneUser: "",            
            }}

            validationSchema={validationSchema}
            onSubmit={(values, {setSubmitting}) => {
                onSubmit(values);
                setSubmitting(false);

            }}
        >
        {({isSubmitting}) => (
            <Form className="form-conteiner">

                <h1>Delivery registration</h1>

                <div className="form-item">
                    <label htmlFor="nameUser">First Name</label>
                    <Field type="nameUser" name="nameUser"/>                    
                </div>
                <ErrorMessage name="nameUser" component="div" />
                <div className="form-item">
                    <label htmlFor="surnameUser">Last Name</label>
                    <Field type="surnameUser" name="surnameUser"/>                    
                </div>
                <ErrorMessage name="surnameUser" component="div" />
                <div className="form-item">
                    <label htmlFor="ageUser">Age</label>
                    <Field type="ageUser" name="ageUser"/>                    
                </div>
                <ErrorMessage name="ageUser" component="div" />
                <div className="form-item">
                    <label htmlFor="adressUser">Address</label>
                    <Field type="adressUser" name="adressUser"/>                    
                </div>
                <ErrorMessage name="adressUser" component="div" />
                <div className="form-item">
                    <label htmlFor="phoneUser">Phone</label>
                    <Field type="phoneUser" name="phoneUser"/>
                    
                </div>
                <ErrorMessage name="phoneUser" component="div" />
                <button className="button_modal" type="submit" disabled={isSubmitting}>Send</button>
                <button className="modal-close" type="button" onClick={onClose}>X</button>
            </Form>
        )

        }




        </Formik>
    </>
)



export default LoginForm