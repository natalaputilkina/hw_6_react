import { configureStore } from '@reduxjs/toolkit';
import shopReducer, { initializeCart, initializeFavorite } from './shopSlice';
import productsReducer, { fetchProducts } from './productsSlice';

const store = configureStore({
  reducer: {
    products: productsReducer,
    shop: shopReducer,
  },
});

const cart = JSON.parse(localStorage.getItem('cart')) || [];
const favorite = JSON.parse(localStorage.getItem('favorite')) || [];

store.dispatch(initializeCart(cart));
store.dispatch(initializeFavorite(favorite));

export default store;









