import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider } from 'react-router-dom'
import {router} from './router'
import { Provider } from 'react-redux';
import store from './store';
import { ViewProvider } from './ViewContext.jsx';



ReactDOM.createRoot(document.getElementById('root')).render(

  <Provider store={store}>
    <ViewProvider>
      <RouterProvider router={router}/>
    </ViewProvider>
   
  </Provider>
  
);
